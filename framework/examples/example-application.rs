


use serde_json::Value;

use new_home_application::app::ApplicationBuilder;
use new_home_application::communication::{MethodResult};
use new_home_application::method::{Method, MethodCallable};

struct MyMethod {}

impl Method for MyMethod {
    fn name(&self) -> String {
        String::from("my_method")
    }

    fn description(&self) -> String {
        Default::default()
    }

    fn help(&self) -> String {
        Default::default()
    }
}

impl MethodCallable for MyMethod {
    type ArgumentsType = ();

    fn secure_call(&mut self, _: String, _: Self::ArgumentsType) -> MethodResult {
        println!("called");

        MethodResult {
            code: 0,
            message: Value::String(String::from("called")),
        }
    }
}

fn create_my_method() -> MyMethod {
    MyMethod {}
}

fn main() {
    let mut app = ApplicationBuilder::new();

    app.with(Box::new(create_my_method));

    match app.run(String::from("127.0.0.1:1337")) {
        Err(error) => {
            println!("{}", error);
        }
        _ => {}
    }
}
