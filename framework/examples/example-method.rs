extern crate new_home_application;
#[macro_use]
extern crate serde_json;



use serde::Deserialize;


use new_home_application::communication::{MethodResult};
use new_home_application::method::{Method, MethodCallable};

/// The arguments used in the callable later
#[derive(Deserialize)]
struct ExampleArguments {
    /// The word that should be converted
    pub word: String,

    /// Determines if the word will be converted to uppercase
    pub uppercase: bool,
}

struct ExampleMethod;

impl Method for ExampleMethod {
    fn name(&self) -> String {
        String::from(r#"example_method"#)
    }

    fn description(&self) -> String {
        String::from(r#"Can convert a word into uppercase"#)
    }

    fn help(&self) -> String {
        String::from(r#"Will convert the word to uppercase if uppercase flag is set"#)
    }
}

impl MethodCallable for ExampleMethod {
    type ArgumentsType = ExampleArguments;

    fn secure_call(&mut self, name: String, arguments: Self::ArgumentsType) -> MethodResult {
        MethodResult {
            code: 0,
            message: json!({
                "method": name,
                "word": arguments.word,
                "uppercase": arguments.uppercase
            }),
        }
    }
}

fn main() {
    let mut ex = ExampleMethod;
    let arguments = ExampleArguments {
        word: "Hello world".to_string(),
        uppercase: false,
    };

    println!(
        "{:?}",
        ex.secure_call(String::from("example_method"), arguments)
    );
}
