//! # New Home Application
//!
//! This is the New Home Application framework used to create application for the New Home project.
//! The New Home project aims to make a Smart Home (more or less) simple for people who are willing
//! to tinker a bit around with electronics and software (Or knows someone who is).
//!
//! For a more detailed description on what this project exactly is, for who it is and how to setup
//! a "New Home" follow the documentation in the [main project](https://gitlab.com/y_software/new-home-core)
//!
//! # Usage
//!
//! To implement a [Method](new_home_application::method::Method) it is recommended to use the
//! new_home_application_macro crate.
//!
//! To get the whole application up and running you will have to build the application using the
//! [ApplicationBuilder](new_home_application::app::ApplicationBuilder). This will help you configure
//! the application and get it running. For a more detailed implementation on how to use the builder
//! you can take a look into the examples/example-application.rs file.
//!
//! # Implementation
//!
//! In order to be fully complient with the New Home system and especially with the Core and UI, you
//! will have to create implement some methods. These methods are:
//!
//! | Name            | Scope | Description                                                                                             |
//! |-----------------|-------|---------------------------------------------------------------------------------------------------------|
//! | `get_script`    | Core  | Provides a JavaScript file. Has to be in the `message.script` key.                                      |
//! | `device_action` | UI    | Provides the info on what should happen when clicking on the device button in the room/device overview. |
//! | `config_root`   | UI    | Provides the info on what should happen when the application button is clicked.                         |
//!
//! # Examples
//!
//! An example on how to implement a method with the macro is in the new_home_application_macro crate.
//!
//! An example on how to implement the same structure is in the examples/example-method.rs
//!
//! The short version (without the main part) is also here:
//!
//! ```rust
//! use serde_json::Value;
//! use new_home_application::method::{MethodCallable, Method, MethodError};
//! use new_home_application::communication::{MethodCall, MethodResult};
//! use std::collections::HashMap;
//!
//! struct ExampleMethod {
//!     pub word: Value,
//!
//!     pub uppercase: Value,
//! }
//!
//! impl Method for ExampleMethod {
//!     fn name(&self) -> String {
//!         String::from(r#"example_method"#)
//!     }
//!
//!     fn description(&self) -> String {
//!         String::from(r#"Can convert a word into uppercase"#)
//!     }
//!
//!     fn help(&self) -> String {
//!         String::from(r#"Will convert the word to uppercase if uppercase flag is set"#)
//!     }
//!
//!     fn set_arguments(
//!         &mut self,
//!         arguments: HashMap<String, Value>,
//!     ) -> Result<(), MethodError> {
//!         match arguments.get("word") {
//!             Some(value) => {
//!                 self.word = value.clone();
//!             }
//!             _ => {
//!                 return Err(MethodError::message(
//!                     "Missing value for word",
//!                 ));
//!             }
//!         }
//!         self.uppercase = arguments
//!             .get("uppercase")
//!             .unwrap_or(&serde_json::Value::from(false))
//!             .clone();
//!         Ok(())
//!     }
//! }
//!
//! impl MethodCallable for ExampleMethod {
//!     fn call(&mut self, _: MethodCall) -> MethodResult {
//!         unimplemented!()
//!     }
//! }
//! ```
//!
//! This is the same example the derived (macro) one and by that I mean, that this is the
//! `cargo expand` result.
//!

extern crate serde;
#[macro_use]
extern crate serde_json;

pub mod app;
pub mod communication;
pub mod method;
