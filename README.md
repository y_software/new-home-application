[![Pipeline Status](https://gitlab.com/Y_Software/new-home-application/badges/develop/pipeline.svg)](https://gitlab.com/Y_Software/new-home-application/pipelines)
[![creates.io](https://img.shields.io/crates/v/new-home-application.svg?style=flat-square&logo=rust)](https://crates.io/crates/new-home-application)

# New Home application

This is the New Home Application framework used to create application for the New Home project.
The New Home project aims to make a Smart Home (more or less) simple for people who are willing
to tinker a bit around with electronics and software (Or knows someone who is).

For a more detailed description on what this project exactly is, for who it is and how to setup
"New Home" follow the documentation in the [main project](https://gitlab.com/y_software/new-home-core)

## Setup

As this is only the application framework, and the setup for every application may be different, take a look at the 
application you want to install if it provides a setup guide. If not try to ask the author of the application if he/she
can you provide instructions on how to set the application up.

For more general instructions on how to set up the project with its core etc. you can read the "Setup" section in the
[main project](https://gitlab.com/y_software/new-home-core)

## Documentation

You can find the documentation for this project on [docs.rs](https://docs.rs/new-home-application). If you have no 
network connection, or the site is not reachable you can run the `cargo doc` command.
